variable "datacenter" {
  type = string
}
variable "cluster" {
  type = string
}
variable "portgroup" {
  type = string
}
variable "datastore" {
  type = string
}
variable "vm_template" {
  type = string
}
variable "host_name" {
  type = string
}
variable "address_v4" {
  type = string
}
variable "gateway_v4" {
  type = string
}

# TBD: DNS Servers (IPv4)
#variable "dns_servers_v4" {
#  type = map(list(string))
#  default = {
#    "."    = ["8.8.8.8"]
#  }
#} 
